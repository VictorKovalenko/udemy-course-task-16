import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Course task 16',
      theme: ThemeData(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  late AnimationController _sizeController;
  late Animation<double> _sizeAnimation;
  bool value = true;

  @override
  void initState() {
    super.initState();
    _sizeController = AnimationController(vsync: this, duration: Duration(seconds: 1));
    _sizeAnimation = Tween<double>(begin: 100, end: 500).animate(_sizeController)
      ..addListener(() {
        setState(() {});
      });

    _sizeController.addListener(() {
      setState(() {});
      _sizeController.forward();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [

          AnimatedContainer(
            decoration: BoxDecoration(
              color: Colors.blue,
              boxShadow: [BoxShadow(
                color: Colors.black,
                blurRadius: !value?30:0,
              )],
            ),

            duration: Duration(seconds: 1),
            height: value ? 100.0 : 200.0,
            width: double.infinity,
          ),
          const SizedBox(height: 50.0),
          Center(
            child: ElevatedButton(
              onPressed: () {
                setState(() {
                  value = !value;
                });
              },
              child: Text(''),
            ),
          ),
        ],
      ),
    );
  }
}

//class MyAppState extends State<MyApp> with TickerProviderStateMixin {
//   double _height = 50.0;
//   double _width = 20.0;
//   var _color = Colors.blue;
//
//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//         body: new Center(
//           child: new Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               new AnimatedSize(
//
//                 curve: Curves.fastOutSlowIn, child: new Container(
//                 width: _width,
//                 height: _height,
//                 color: _color,
//               ), vsync: this, duration: new Duration(seconds: 2),),
//               new Divider(height: 35.0,),
//               new Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: <Widget>[
//                   new IconButton(
//                       icon: new Icon(Icons.arrow_upward, color: Colors.green,),
//                       onPressed: () =>
//                           setState(() {
//                             _color = Colors.green;
//                             _height = 95.0;
//                           })),
//                   new IconButton(
//                       icon: new Icon(Icons.arrow_forward, color: Colors.red,),
//                       onPressed: () =>
//                           setState(() {
//                             _color = Colors.red;
//                             _width = 45.0;
//                           })),
//                 ],
//               )
//             ],)
//           ,)
//     );
//   }
// }
